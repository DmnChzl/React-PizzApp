import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Notification from '../Notification';

describe('Notification: Component', () => {
  it('Should Component Renders', () => {
    const { getByRole, queryByText } = render(
      <div>
        <Notification render={({ setMessage }) => (
          <button onClick={() => setMessage('Lorem Ipsum')}>Click Me</button>
        )} />
      </div>
    );

    const button = getByRole('button');

    fireEvent.click(button);

    expect(queryByText('Lorem Ipsum')).toBeInTheDocument();
  });
});
