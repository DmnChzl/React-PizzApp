import React, { useState, useReducer } from 'react';
import { func } from 'prop-types';
import classNames from 'classnames';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { pswdAccount, deleteAccount } from '../../redux/account';
import { Row, Column, Field, Button } from '../ui';

function reducer(state, { type, payload }) {
  switch (type) {
    case 'set':
      return {
        ...state,
        ...payload
      };

    case 'reset':
      return {
        ...state,
        [payload]: ''
      };

    default:
      return state;
  }
}

function useInput(initialState) {
  const [value, setValue] = useState(initialState);

  return [
    value,
    e => setValue(e.target.value)
  ];
}

function Credentials(props) {
  const history = useHistory();
  const dispatch = useDispatch();
  const [hidden, setHidden] = useState(true);
  const [oldPswd, setOldPswd] = useInput('');
  const [newPswd, setNewPswd] = useInput('');
  const [repeatPswd, setRepeatPswd] = useInput('');
  const [errors, dispatchErrors] = useReducer(reducer, {
    oldPswd: '',
    newPswd: '',
    repeatPswd: ''
  });

  const handleSubmit = event => {
    const { setMessage, colorizeRed, colorizeGreen } = props;

    event.preventDefault();
    
    if (!oldPswd || oldPswd.length < 6) {
      return dispatchErrors({ type: 'set', payload: { oldPswd: 'Password Error !' }});
    }

    dispatchErrors({ type: 'reset', payload: 'oldPswd' });
    
    if (!newPswd || newPswd.length < 6) {
      return dispatchErrors({ type: 'set', payload: { newPswd: 'New Password Error !' }});
    }

    dispatchErrors({ type: 'reset', payload: 'newPswd' });
    
    if (!repeatPswd || repeatPswd.length < 6 || repeatPswd !== newPswd) {
      return dispatchErrors({ type: 'set', payload: { repeatPswd: 'Repeat Password Error !' }});
    }

    dispatchErrors({ type: 'reset', payload: 'repeatPswd' });

    dispatch(pswdAccount({ oldPswd, newPswd }))
      .then(() => {
        colorizeGreen();
        setMessage('Success !');
      })
      .catch(({ message }) => {
        colorizeRed();
        setMessage(message);
      });
  };

  const handleClick = () => {
    const { setMessage, colorizeRed } = props;

    dispatch(deleteAccount())
      .then(() => {
        history.push('/');
      })
      .catch(({ message }) => {
        colorizeRed();
        setMessage(message);
      });
  };

  return (
    <div className="card">
      <header className="card-header">
        <p className="card-header-title">
          Credentials
        </p>
        <a className="card-header-icon" href="#credentials" aria-label="toggle" onClick={() => setHidden(!hidden)}>
          <span className="icon">
            <i className="fas fa-angle-down" />
          </span>
        </a>
      </header>

      <div id="credentials" className={classNames('card-content', { 'is-hidden': hidden })}>
        <form onSubmit={handleSubmit}>
          <Field
            label="Password"
            name="oldPswd"
            type="password"
            placeholder="**********"
            defaultValue={oldPswd}
            onChange={setOldPswd}
            error={errors.oldPswd} />

          <Field
            label="New Password"
            name="newPswd"
            type="password"
            placeholder="**********"
            defaultValue={newPswd}
            onChange={setNewPswd}
            error={errors.newPswd} />
            
          <Field
            label="Repeat New Password"
            name="repeatPswd"
            type="password"
            placeholder="**********"
            defaultValue={repeatPswd}
            onChange={setRepeatPswd}
            error={errors.repeatPswd} />

          <Row>
            <Column size={6}>
              <Button type="submit" color="success" filled>
                <strong>Save</strong>
              </Button>
            </Column>
            <Column size={6}>
              <Button color="danger" filled onClick={handleClick}>
                <strong>Delete</strong>
              </Button>
            </Column>
          </Row>
        </form>
      </div>
    </div>
  );
}

Credentials.propTypes = {
  setMessage: func.isRequired,
  colorizeRed: func.isRequired,
  colorizeGreen: func.isRequired
};

export default Credentials;
